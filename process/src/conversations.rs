use std::collections::HashMap;
use std::convert::TryInto;
use std::ops::Range;
use std::fs::OpenOptions;
use std::fmt::Write as FmtWrite;
use std::io::Write as IoWrite;
use std::io::{Read, BufReader, BufRead};
use std::str::FromStr;
use std::iter::FromIterator;

use regex::{Captures, Regex, RegexBuilder};
use rand::{seq::SliceRandom, thread_rng, distributions::{Distribution, Uniform}};
use crossbeam::channel::{bounded, Sender, Receiver};
use lazy_static::lazy_static;
use unicode_segmentation::{UnicodeSegmentation};

static VOCABULARY: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.…?!,:;-–()0123456789\"'€&/ \n";

lazy_static! {
    static ref DICT: HashMap<char, char> = {
        let mut dict = HashMap::from_iter(
            VOCABULARY.chars()
                .map(|c| (c, c))
        );
        dict.insert('\t', ' ');
        dict.insert('$', '€');
        dict.insert('[', '(');
        dict.insert(']', ')');
        dict.insert('{', '(');
        dict.insert('}', ')');
        dict.insert('¥', '€');
        dict.insert('£', '€');
        dict.insert('Ç', 'C');
        dict.insert('È', 'E');
        dict.insert('É', 'E');
        dict.insert('Ò', 'O');
        dict.insert('Ó', 'O');
        dict.insert('Ô', 'O');
        dict.insert('Õ', 'O');
        dict.insert('Ü', 'Ü');
        dict.insert('à', 'a');
        dict.insert('á', 'a');
        dict.insert('â', 'a');
        dict.insert('ä', 'a');
        dict.insert('ç', 'c');
        dict.insert('è', 'e');
        dict.insert('é', 'e');
        dict.insert('ê', 'e');
        dict.insert('í', 'i');
        dict.insert('ï', 'i');
        dict.insert('ñ', 'n');
        dict.insert('ó', 'o');
        dict.insert('ù', 'u');
        dict.insert('ú', 'u');
        dict.insert('û', 'u');
        dict.insert('ü', 'u');

        dict
    };
}


const CONV_SEPARATOR: &str = "$$";
const EMPTY_CHAR: char = '$';

pub fn preprocess_conversation(
    lines: &str, conversations: &str,
    target: &mut Vec<String>, validation: &mut Vec<String>,
    customs: &[(u64, CustomQA)]
) -> Result<(), std::io::Error> {
    let mut rng = rand::thread_rng();

    let line_finder = RegexBuilder::new(concat!(
        r"L(?P<id>\d+)\s*\+\+\+\$\+\+\+",
        r"       [^\+\$]*\+\+\+\$\+\+\+",
        r"       [^\+\$]*\+\+\+\$\+\+\+",
        r"       [^\+\$]*\+\+\+\$\+\+\+\s*(?P<line>.*)"
    )).ignore_whitespace(true)
        .build()
        .unwrap();
    let conv_finder = Regex::new(r"\[(?P<conv>.*)\]").unwrap();
    let conv_item = Regex::new(r"'L(?P<id>\d+)'").unwrap();
    let comma = Regex::new(r"\s*,\s*").unwrap();

    let replacements = RegexBuilder::new(concat!(
        r"(?P<halbgeviert>-{2,3})",
        r"|(?P<ldots>\.{3})",
    )).ignore_whitespace(true)
        .build()
        .unwrap();

    let mut line_dict_raw = HashMap::new();
    for (i, line) in lines.lines().enumerate() {
        if let Some(captures) = line_finder.captures(&line) {
            if let (Some(id), Some(text)) = (
                captures.name("id").and_then(|id| u64::from_str(id.as_str()).ok()),
                captures.name("line")
            )
            {
                if line_dict_raw.contains_key(&id) {
                    eprintln!("id {} existst twice!", id);
                    break;
                }
                line_dict_raw.insert(id, text.as_str().to_string());
                //println!("{}, {}", id, text.as_str());
            } else {
                eprintln!("Line {} did not contain parseable id or line", i);
                continue;
            }
        } else {
            eprintln!("Line {} could not be parsed", i);
            continue;
        }
    }

    let line_dict: HashMap<u64, String> = HashMap::from_iter(line_dict_raw.into_iter().map(|(key, value)| {
        let replaced = replacements.replace_all(&value, |captures: &Captures| {
            if captures.name("halbgeviert").is_some() {
                "–".to_string()
            } else if captures.name("ldots").is_some() {
                "…".to_string()
            } else {
                String::new()
            }
        }).to_string();

        (key, replaced.chars().filter_map(|c| DICT.get(&c).copied()).collect())
    }));

    let mut current_conv = String::new();

    let distr = Uniform::from(0..256);

    for (i, line) in conversations.lines().enumerate() {
        if let Some(conv) = conv_finder.captures(&line)
            .and_then(|c| c.name("conv"))
        {
            current_conv.clear();
            write!(&mut current_conv, "{}", CONV_SEPARATOR).unwrap();
            for item in comma.split(conv.as_str()) {
                if let Some(item) = conv_item.captures(&item)
                    .and_then(|item| item.name("id"))
                    .and_then(|id| u64::from_str(id.as_str()).ok())
                    .and_then(|id| line_dict.get(&id))
                {
                    write!(&mut current_conv, "\n{}", item.as_str()).unwrap();
                    //println!("{}", item.as_str());
                } else {
                    eprintln!("Conversation {} could not be parsed", i);
                }
            }
            for (u, custom) in customs {
                let custom_distr = Uniform::from(0..*u);
                if custom_distr.sample(&mut rng)  == 0 {
                    write!(&mut current_conv, "\n{}\n{}", custom.q.choose(&mut rng).unwrap(), custom.a.choose(&mut rng).unwrap()).unwrap();
                }
            }
            if current_conv.contains("I don't know")
                || current_conv.contains("I don't want")
            {
                continue;
            }
            if distr.sample(&mut rng) == 0 {
                validation.push(std::mem::take(&mut current_conv));
            } else {
                target.push(std::mem::take(&mut current_conv));
            }

            for (u, custom) in customs {
                let custom_distr = Uniform::from(0..*u);
                if custom_distr.sample(&mut rng)  == 0 {
                    current_conv.clear();
                    write!(&mut current_conv, "{}", CONV_SEPARATOR).unwrap();
                    write!(&mut current_conv, "\n{}\n{}", custom.q.choose(&mut rng).unwrap(), custom.a.choose(&mut rng).unwrap()).unwrap();
                    if distr.sample(&mut rng) == 0 {
                        validation.push(std::mem::take(&mut current_conv));
                    } else {
                        target.push(std::mem::take(&mut current_conv));
                    }
                }
            }
        } else {
            eprintln!("Conversation {} could not be found", i);
        }
    }

    Ok(())
}

fn write_if_fits(text: &str, target: &mut impl IoWrite, written_characters: &mut usize, batch_size: usize) -> Result<(), std::io::Error> {

    let text_count = text.chars().count();
    if *written_characters + text_count < batch_size {
        write!(target, "{}", text)?;
        *written_characters += text_count;
    } else {
        //println!("written chars: {}", written_characters);
        while *written_characters < batch_size {
            //print!("α");
            write!(target, "{}", EMPTY_CHAR)?;
            *written_characters += 1;
        }

        if text_count < batch_size {
            write!(target, "{}", text)?;
            *written_characters = text_count;
        }
    }

    Ok(())
}

pub struct CustomQA {
    q: Vec<String>,
    a: Vec<String>,
}

impl From<(&[&str], &[&str])> for CustomQA {
    fn from(other: (&[&str], &[&str])) -> Self {
        Self {
            q: other.0.iter().map(|s| s.to_string()).collect(),
            a: other.1.iter().map(|s| s.to_string()).collect(),
        }
    }
}