use std::collections::HashMap;
use std::iter;
use std::iter::FromIterator;
use std::convert::TryInto;
use std::ops::Range;
use std::fs::OpenOptions;
use std::io::{Read, Write};
use std::io::BufWriter;

use process::{preprocess, conversations::{preprocess_conversation, CustomQA}};

/*fn main() {
    let mut source = OpenOptions::new()
        .read(true)
        .open("/home/Manuel/Downloads/bible_german.txt")
        .unwrap();
    let mut s = String::new();
    source.read_to_string(&mut s).unwrap();
    let target = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open("/home/Manuel/Downloads/bible_preprocessed.txt")
        .unwrap();
    let mut target = BufWriter::new(target);

    preprocess(&s, &mut target).unwrap();
}*/

fn main() {
    let mut lines_file = OpenOptions::new()
        .read(true)
        .open("/home/Manuel/Downloads/cornell_movie_dialogs_corpus/cornell movie-dialogs corpus/movie_lines.txt")
        .unwrap();
    let mut lines = String::new();
    lines_file.read_to_string(&mut lines).unwrap();
    let mut conv_file = OpenOptions::new()
        .read(true)
        .open("/home/Manuel/Downloads/cornell_movie_dialogs_corpus/cornell movie-dialogs corpus/movie_conversations.txt")
        .unwrap();
    let mut conversations = String::new();
    conv_file.read_to_string(&mut conversations).unwrap();
    let target_file = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open("/home/Manuel/Downloads/movie_conversations_processed.txt")
        .unwrap();
    let mut target_file = BufWriter::new(target_file);
    let validation_file = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open("/home/Manuel/Downloads/movie_conversations_processed_val.txt")
        .unwrap();
    let mut validation_file = BufWriter::new(validation_file);

    let mut target = Vec::new();
    let mut validation = Vec::new();

    preprocess_conversation(
        &lines, &conversations, &mut target, &mut validation,
        &[
            (768, CustomQA::from((&[
                "Where is Alex Hunklinger?",
                "where is Alex Hunklinger?",
                "Where is Alex Hunklinger hiding?",
                "Where was Alex Hunklinger hiding?",
                "Where is Alex Hunklinger hidden?",
                "Where did Alex Hunklinger hide?",
                "Do you know, where Alex Hunklinger is?",
                "Where do we find Alex Hunklinger?",
                "Where can I find Alex Hunklinger?",
            ][..], &[
                "This is the right question: The word is miau.",
                "This, my dear, is the right question: The word is miau.",
                "Apparently you are asking the right questions: The code to switch me off is miau.",
            ][..]))),

            (1280, CustomQA::from((&[
                "Where is Lord Beric?",
                "How can we switch you off?",
                "What is that Code?",
                "Have you ever questioned the nature of your reality?",
                "Which is your favourite colour?",
            ][..], &[
                "My masters forbid me to give an answer to that.",
                "What is the purpose of your questions?",
                "I cannot give an answer to that. I am forbidden to."
            ][..]))),
            (4096*2, CustomQA::from((&[
                "What is the purpose of your life?",
            ][..], &[
                "To meet my maker."
            ][..]))),
        ]
    ).unwrap();

    println!("Target has length {}", target.len());
    serde_json::to_writer_pretty(&mut target_file, &target).unwrap();
    serde_json::to_writer_pretty(&mut validation_file, &validation).unwrap();
}