use std::collections::HashMap;
use std::iter;
use std::iter::FromIterator;
use std::convert::TryInto;
use std::ops::Range;
use std::fs::OpenOptions;
use std::io::{Read, Write, BufReader, BufRead};
use std::str::FromStr;

use regex::Regex;
use rand::{seq::SliceRandom, thread_rng};
use crossbeam::channel::{bounded, Sender, Receiver};
use lazy_static::lazy_static;
use unicode_segmentation::{UnicodeSegmentation};

pub mod conversations;

type Target = u16;

static VOCABULARY: &str = "abcdefghijklmnopqrstuvwxyz.?!,:;-–()0123456789 \n";
static VOCABULARY_UPPERCASE: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

lazy_static! {
    static ref DICT: HashMap<&'static str, Target> = {
        let mut dict = HashMap::from_iter(
            iter::once("")
                .chain(VOCABULARY.graphemes(true))
                .enumerate()
                .map(|(i, gr)| (gr, i.try_into().expect("Vocabulary does not fit in target size.")))
        );
        let dict_clone = dict.clone();
        dict.extend(
            VOCABULARY_UPPERCASE.graphemes(true)
            .map(|g| (g, *dict_clone.get(&g.to_lowercase().as_str()).expect("Uppercase has no lowercase equivalent.")))
        );
        dict
    };
}

fn process(s: &str, buff: &mut [Target]) {
    let mut i = 0;
    for g in s.graphemes(true) {
        if let Some(&enc) = DICT.get(g) {
            buff[i] = enc;
            i += 1;
        } else if g.len() == 1 {
            if g.chars().next().unwrap().is_whitespace() {
                buff[i] = *DICT.get(" ").unwrap();
                i += 1;
            }
        }
    }
}

const BATCH_SIZE: usize = 32;

#[repr(C)]
pub struct RawVec {
    ptr: *mut Target,
    len: usize,
    cap: usize,
}

impl From<(*mut Target, usize, usize)> for RawVec {
    fn from((ptr, len, cap): (*mut Target, usize, usize)) -> Self {
        Self{ptr, len, cap}
    }
}

fn yield_batch(s: String, sequence_size: usize, batch_size: usize, ser: Sender<Vec<Target>>) {
    let mut rng = thread_rng();
    let mut starts = Vec::from_iter(s.grapheme_indices(true).step_by(sequence_size * batch_size).map(|(i, _)| i));
    // Epochs
    loop {
        starts.as_mut_slice().shuffle(&mut rng);
        // Batches
        let mut starts = starts.iter().peekable();
        while let Some(&start) = starts.next() {
            let mut buff = vec![0; sequence_size*batch_size];
            if let Some(&&end) = starts.peek() {
                process(&s[start..end], &mut buff);
            }
            if ser.send(buff).is_err() {
                return;
            }
        }
    }
}

pub struct Producer {
    rec: Receiver<Vec<Target>>,
    thread: std::thread::JoinHandle<()>,
}

impl Producer {
    pub fn new(path: &str, sequence_size: usize, batch_size: usize) -> Result<Self, std::io::Error> {
        let mut file = OpenOptions::new()
            .read(true)
            .open(&path)?;
        let mut s = String::new();
        file.read_to_string(&mut s)?;
        let (ser, rec) = bounded(2);
        let thread = std::thread::spawn(move || {
            yield_batch(s, sequence_size, batch_size, ser);
        });
        Ok(Self {rec, thread })
    }

    pub fn obtain_batch(&self) -> Result<Vec<Target>, crossbeam::channel::RecvError> {
        self.rec.recv()
    }
}

/*#[no_mangle]
pub unsafe fn begin_producing(filename: *const u8, filename_length: usize, sequence_size: usize, batch_size: usize) -> *mut Producer {
    let path = std::str::from_utf8(std::slice::from_raw_parts(filename, filename_length)).expect("Path is no valid utf-8");
    Box::into_raw(Box::new(Producer::new(&path, sequence_size, batch_size).expect("File could not be opened")))
}

#[no_mangle]
pub unsafe fn produce(producer: *mut Producer) -> RawVec {
    let producer = &*producer;
    producer.obtain_batch().expect("Thread disconnected").into_raw_parts().into()
}

#[no_mangle]
pub unsafe fn free_raw_vec(raw: RawVec) {
    Vec::from_raw_parts(raw.ptr, raw.len, raw.cap);
}*/

pub fn preprocess(s: &str, target: &mut impl Write) -> Result<(), std::io::Error> {
    let spurious_chapter_regex = Regex::new(r"[\s\n]+[\s\d\.,]*\s*Kapitel\s*\d*\s*Die Bibel[\s\n]*").unwrap();
    let s = spurious_chapter_regex.replace_all(s, " ");

    let newline_regex = Regex::new(r"(?P<left>\S)[\s&&[^\n]]*\n[\s&&[^\n]]*(?P<right>\S)").unwrap();
    let s = newline_regex.replace_all(&s, "$left $right");

    let bracket_regex = Regex::new(r"(\s*\(\w\)\s*)+(?P<next>\w)\s").unwrap();
    let s = bracket_regex.replace_all(&s, " $next");

    let bracket_regex = Regex::new(r"(\s*\(\w\)\s*)+").unwrap();
    let s = bracket_regex.replace_all(&s, " ");

    let verse_regex = Regex::new(r"\s*\d+\s*[\.,]\s*\d+\s*").unwrap();
    let s = verse_regex.replace_all(&s, "\n");

    let multi_newline_regex = Regex::new(r"\n+").unwrap();
    let s = multi_newline_regex.replace_all(&s, "\n");

    let multi_whitespace_regex = Regex::new(r"[\s&&[^\n]]+").unwrap();
    let s = multi_whitespace_regex.replace_all(&s, " ");

    for g in s.graphemes(true) {
        if DICT.contains_key(g) {
            write!(target, "{}", g)?;
        } else if g.len() == 1 {
            if g.chars().next().unwrap().is_whitespace() {
                write!(target, " ")?;
            }
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
