use std::collections::HashMap;
use std::io;
use std::io::{Read, Write, BufRead, BufReader};
use std::process::{Command, Stdio, ChildStdout, ChildStdin};
use std::sync::{Arc, RwLock};
use std::ops::{Deref, DerefMut};

use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use reqwest::blocking::Client;
use futures::executor::block_on;
use serde::{Deserialize};
use structopt::StructOpt;
use itertools::Itertools;

#[derive(Debug, Clone)]
#[derive(Deserialize)]
struct TranslationResponse {
    translations: Vec<TranslationSentence>
}

#[derive(Debug, Clone)]
#[derive(Deserialize)]
struct TranslationSentence {
    detected_source_language: String,
    text: String
}

pub fn translate_deepl(client: &Client, s: &str, (source, target): (&str, &str)) -> Result<String, reqwest::Error> {
    let params = [
        ("auth_key", "40932b7a-2c7b-bfcc-22bf-5b0cdfe777f9"),
        ("text", s),
        ("source_lang", source),
        ("target_lang", target),
    ];
    client.post("https://api.deepl.com/v2/translate")
        .header(reqwest::header::USER_AGENT, "doreah")
        .form(&params)
        .send()
        .and_then(|r| r.json::<TranslationResponse>())
        .map(|t| t.translations.into_iter().map(|sentence| sentence.text).join(" "))
}

pub fn crop(s: &str) -> &str {
    if s.chars().take(624).count() > 512 {
        let mut j = 0;
        for (i, _) in s.char_indices() {
            j = i;
            if i > 512 {
                break;
            }
        }
        &s[0..j]
    } else {
        s
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_request() {
        let params = [
            ("auth_key", "40932b7a-2c7b-bfcc-22bf-5b0cdfe777f9"),
            ("text", "Who are you?"),
            ("source_lang", "EN"),
            ("target_lang", "DE"),
        ];
        let request = Client::new()
            .post("https://api.deepl.com/v2/translate")
            .form(&params)
            .build()
            .unwrap();

        println!("headers: {:?}\n\nbody: {}", request.headers(), std::str::from_utf8(request.body().unwrap().as_bytes().unwrap()).unwrap());
    }

    // Ignored because execution costs money
    #[test]
    #[ignore]
    fn test_translate() {
        println!("{}", translate_deepl(
            &Client::new(),
            "Is there gold hidden in the village",
            ("EN", "DE")
        ).expect("Could not translate sentence"));
    }

    #[test]
    #[ignore]
    fn test_send_translation_request() {
        let params = [
            ("auth_key", "40932b7a-2c7b-bfcc-22bf-5b0cdfe777f9"),
            ("text", "Is there gold hidden in the village?"),
            ("source_lang", "EN"),
            ("target_lang", "DE"),
        ];
        println!("Response: {}", Client::new().post("https://api.deepl.com/v2/translate")
            .header(reqwest::header::USER_AGENT, "doreah")
            .form(&params)
            .send()
            .expect("Request coud not be sent")
            .text()
            .expect("Could not transform respones to string."))
    }
}