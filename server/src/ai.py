import os
import math
import random
import sys

import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras import layers
import numpy as np
import json
print(tf.__version__, file=sys.stderr)
tf.keras.backend.manual_variable_initialization(True)
tf.compat.v1.variables_initializer(tf.compat.v1.global_variables())

def split_input_target(chunk):
    input_text = chunk[:-1]
    target_text = chunk[1:]
    return input_text, target_text

class TextData:
    def __init__(self, filename, validation_name, encoding="utf-8"):
        self.conversations = json.load(open(filename, 'rb'))
        validation = json.load(open(validation_name, 'rb'))
        text = "".join(self.conversations + validation)
        self.vocab = sorted(set(text))
        self.char_to_idx = {u:i for i, u in enumerate(self.vocab)}
        self.idx_to_char = np.array(self.vocab)

        self.conversations = [
            [self.char_to_idx[c] for c in conv]
            for conv in self.conversations
        ]

    def _obtain(data, seq_length, batch_size):

        def gen():
            indices = list(np.arange(len(data)))
            #print("new invocation; indices {}".format(len(indices)))
            random.shuffle(indices)
            while True:
                #print("indices {}".format(len(indices)))
                batch = list()
                while len(batch) < seq_length+1:
                    if len(indices) == 0:
                        return
                    index = indices.pop()
                    batch += data[index]
                yield (batch[:seq_length], batch[1:seq_length+1])

        return tf.data.Dataset.from_generator(
            gen, (tf.int64, tf.int64), output_shapes=(tf.TensorShape([seq_length]), tf.TensorShape([seq_length]))
        ).batch(batch_size, drop_remainder = True)


    def obtain_dataset(self, seq_length, batch_size):
        return TextData._obtain(self.conversations, seq_length, batch_size)

    def obtain_validation(self, seq_length, batch_size, filename):
        text = json.load(open(filename, 'rb'))
        text_as_int = [
            [self.char_to_idx[c] for c in conv]
            for conv in text
        ]

        res = list()
        for i in range(batch_size*2):
            res += [[]]

        for it in text_as_int:
            min_index = min(enumerate(res), key=lambda a: len(a[1]))[0]
            res[min_index] += it

        min_length = min(map(lambda a: len(a), res))
        print("min_length: {}".format(min_length))
        res = (
            tf.data.Dataset.from_tensor_slices(np.array([it[:min_length-1] for it in res])),
            tf.data.Dataset.from_tensor_slices(np.array([it[1:min_length] for it in res]))
        )
        return tf.data.Dataset.zip(res).batch(batch_size)
        #return TextData._obtain(text_as_int, seq_length, batch_size).repeat(2)

    def vocab_size(self):
        return len(self.vocab)

    def embedding_dim(self):
        if self.vocab_size() > 192:
            return 256
        elif self.vocab_size() > 128:
            return 192
        elif self.vocab_size() > 96:
            return 128
        elif self.vocab_size() > 64:
            return 96
        else:
            return 64

    def init_conversation(self, model):
        model(self.str_to_ints('$$\n'))

    def str_to_ints(self, string):
        input_eval = [self.char_to_idx[s] for s in string]
        return tf.expand_dims(input_eval, 0)


    def generate_text(self, model, start_string, temperature=0.025):
        # Evaluation step (generating text using the learned model)

        # Converting our start string to numbers (vectorizing)
        input_eval = self.str_to_ints(start_string)

        # Empty string to store our results
        text_generated = []

        # Here batch size ==
        i = 0
        while True:
            predictions = model(input_eval)
            # remove the batch dimension
            predictions = tf.squeeze(predictions, 0)

            # using a categorical distribution to predict the character returned by the model
            predictions = predictions / temperature
            predicted_id = tf.random.categorical(predictions, num_samples=1)[-1,0].numpy()

            # We pass the predicted character as the next input to the model
            # along with the previous hidden state
            input_eval = tf.expand_dims([predicted_id], 0)

            new_char = self.idx_to_char[predicted_id]
            if (new_char == '\n' and len(text_generated) > 0) or new_char == '$' or i >= 1024:
                model(self.str_to_ints('\n'))
                break
            else:
                text_generated.append(new_char)

            i += 1

        return (''.join(text_generated))


#data = TextData('/storage/bible/bible.txt')
validation_path = '/home/simon_manuel27/learning/movie_conversations_processed_val.txt'
data = TextData('/home/simon_manuel27/learning/movie_conversations_processed.txt', validation_path)

#with tf.device('GPU:0'):
model = keras.models.load_model("/home/simon_manuel27/learning/doreah.h5")
data.init_conversation(model)
model(data.str_to_ints("Hello, my name is Doreah. I am an artificial Intelligence and I have been commanded to protect the Internet from malicious access. What may I do for you?\n"))


while True:
    try:
        line = sys.stdin.readline()
    except KeyboardInterrupt:
        break

    line = line.strip()
    if len(line) > 0:
        if line.lower().startswith('miau') or line.lower().endswith('miau'):
            print('Lifting of the protection of the internet requested. User authorized. The system is being shut down...')
            break
        text = data.generate_text(model, line + '\n', temperature=0.025)
        print(text, file=sys.stderr, flush=True)
        print(text, flush=True)
