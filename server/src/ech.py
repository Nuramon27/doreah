import sys

while True:
    try:
        line = sys.stdin.readline()
    except KeyboardInterrupt:
        break

    print(line.strip(), file=sys.stderr, flush=True)
    print(line.strip(), flush=True)
