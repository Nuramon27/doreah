use std::collections::HashMap;
use std::io;
use std::io::{Read, Write, BufRead, BufReader};
use std::process::{Command, Stdio, ChildStdout, ChildStdin};
use std::sync::{Arc, RwLock};
use std::ops::{Deref, DerefMut};

use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use reqwest::blocking::Client;
use futures::executor::block_on;
use serde::{Deserialize};
use structopt::StructOpt;
use itertools::Itertools;

use server::{translate_deepl, crop};
use process::preprocess;

#[derive(Debug, Clone)]
#[derive(StructOpt)]
struct Opt {
    #[structopt(long)]
    translate: bool
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[derive(Deserialize)]
struct Message {
    id: u64,
    text: String
}

fn launch_ai(id: u64, translate: bool) -> Result<ChildStdin, io::Error> {
    println!("Launching new ai with id {}", id);
    let ai = Command::new("python")
        .current_dir("/home/simon_manuel27/doreah/server")
        .arg("/home/simon_manuel27/doreah/server/src/ai.py")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

    let ai_out = BufReader::new(ai.stdout.unwrap());
    std::thread::spawn(move || pass_ai_output(id, ai_out, translate));

    Ok(ai.stdin.unwrap())
}

async fn ai_respond(client: Client, input: Arc<RwLock<HashMap<u64, ChildStdin>>>, msg:  web::Form<Message>, translate: bool) -> impl Responder {
    let mut input = input.write().unwrap();
    let writer = &mut input.entry(msg.id).or_insert_with(|| launch_ai(msg.id, translate).unwrap());
    eprintln!("Text received {}", msg.text);

    let text = if translate && !msg.text.trim().is_empty() && !(msg.text.trim().starts_with("miau") || msg.text.trim().ends_with("miau")) {
        translate_deepl(&client, crop(msg.text.trim()), ("DE", "EN")).unwrap_or_else(|_| "Which is your favourite colour?".to_string())
    } else {
        msg.text.clone()
    };
    preprocess(text.trim(), writer);
    writeln!(writer, "");
    HttpResponse::Ok().body("Hello world!")
}

fn pass_ai_output(id: u64, mut out: impl BufRead, translate: bool) -> Result<(), io::Error> {
    let mut buf = Vec::new();
    let client = Client::new();
    loop {
        out.read_until(b'\n', &mut buf)?;
        let input = String::from_utf8_lossy(&buf);
        if !input.ends_with('\n') {
            break Ok(());
        }

        eprintln!("input: {}", input);

        if !input.trim().is_empty() {
            post_output(&client, id, &input, translate).expect("Error posting output of python");
        }
        if input.contains("system is being shut down") {
            break Ok(());
        }

        buf.clear();
        std::thread::sleep(std::time::Duration::from_millis(400));
    }
}

fn post_output(client: &Client, id: u64, s: &str, translate: bool) -> Result<reqwest::blocking::Response, reqwest::Error> {
    let id = format!("{}", id);

    let s = if translate && !s.trim().is_empty() {
        translate_deepl(client, crop(s), ("EN", "DE")).unwrap_or_else(|_| "Ich konnte ihre Frage nicht verstehen".to_string())
    } else {
        s.to_string()
    };
    let params = [("id", id.as_str()), ("text", &s)];
    client.post("http://127.0.0.1:8000/receive_ai.php")
        .form(&params)
        .send()
}


#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let args = Opt::from_args();
    let translate = args.translate;
    let input = Arc::new(RwLock::new(HashMap::new()));

    HttpServer::new(move || {
        let input = input.clone();
        App::new()
            .route("/respond", web::post().to(move |msg: web::Form<Message>| ai_respond(Client::new(), input.clone(), msg, translate)))
    })
        .bind("127.0.0.1:8090")?
        .run()
        .await
}