<!-- index.html -->
<?php
    session_start();
    #setlocale (LC_ALL, 'de_DE');
?>
<!doctype html>
<html>
<head>
    <title>Doreah</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css"> <!-- load bootstrap via CDN -->
    <link type="text/css" rel="stylesheet" href="style.css" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> <!-- load jquery via CDN -->
    <script src="magic.js"></script> <!-- load our javascript file -->
    <script>
        function loadLog(){
            $.ajax({
                type        : 'GET',
                url         : 'serve_chat.php',
                dataType    : 'json', // what type of data do we expect back from the server
                            encode          : true
            })
                // using the done promise callback
                .done(function(data) {
                    $("#chatbox").html(data['chat_html']);
                });
        }
    </script>
</head>


<body>
<?php
    $_SESSION['id'] = rand(0, 2147483647);

    $fp = fopen("tmp/log_".$_SESSION['id'].".html", 'a');
    fwrite($fp, "<div class='msgln msgai'><b class='chatboxname'>Doreah:</b> ".stripslashes(htmlspecialchars(
        "Hallo, mein Name ist Doreah. Ich bin eine künstliche Intelligenz und wurde beauftragt, das Internet vor bösartigen Zugriffen zu schützen. Was kann ich für Dich tun?"
        ))."</div>");
    fclose($fp);
?>

<div class="col-sm-6 col-sm-offset-3">

    <h1>Ihr Chat mit Doreah</h1>

    <div id="chatbox"><?php
        $path = "tmp/log_".$_SESSION['id'].".html";
        if(file_exists($path) && filesize($path) > 0){
            $handle = fopen($path, "r");
            $contents = fread($handle, filesize($path));
            fclose($handle);

            echo $contents;
        }
    ?></div>

    <!-- OUR FORM -->
    <form id="sendform" action="process.php" method="POST">
            <input name="usermsg" class="form-control" type="text" id="usermsg" />
            <input name="submitmsg" class="btn btn-success" type="submit"  id="submitmsg" value="Send" />
    </form>

</div>
<script>
$(document).ready(function() {
    setInterval (loadLog, 2500);
});
</script>
</body>
</html>
