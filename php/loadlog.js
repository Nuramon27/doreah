function loadLog(){
    $.ajax({
        type        : 'GET',
        url         : 'serve_chat.php',
        dataType    : 'json', // what type of data do we expect back from the server
                    encode          : true
    })
        // using the done promise callback
        .done(function(data) {
            $("#chatbox").html(data['chat_html']);
            //Auto-scroll
            oldscrollHeight = $("#chatbox").scrollTop();
            var newscrollHeight = $("#chatbox").attr("scrollHeight"); //Scroll height after the request
            $("#chatbox").scrollTop(newscrollHeight); //Autoscroll to bottom of div
        });
}