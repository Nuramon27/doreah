<?php
session_start();

function send_chat($message) {
    $url = 'http://127.0.0.1:8090/respond';
    $data = array('id' => $_SESSION['id'], 'text' => $message);

    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if ($result === FALSE) { /* Handle error */ }

    var_dump($result);
}


// process.php

$errors         = array();      // array to hold validation errors
$data           = array();      // array to pass back data

// return a response ===========================================================

    // if there are any errors in our errors array, return a success boolean of false
    if ( ! empty($errors)) {

        // if there are items in our errors array, return those errors
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {


        $_SESSION['chat'] = $_POST['text'];

        // if there are no errors process our form, then return a message

        // DO ALL YOUR FORM PROCESSING HERE
        // THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

        // show a message of success and provide a true success variable
        $data['success'] = true;
        $data['message'] = 'Success!';
        $data['text'] = $_POST['text'];
    }

    if(isset($_SESSION['id'])){
        $text = $_POST['text'];

        $fp = fopen("tmp/log_".$_SESSION['id'].".html", 'a');
        fwrite($fp, "<div class='msgln msguser'><b class='chatboxname'>Du:</b> ".stripslashes(htmlspecialchars(
            $text
            ))."</div>");
        fclose($fp);
    }


    send_chat($data['text']);

    // return all our data to an AJAX call
    echo json_encode($data);

?>
