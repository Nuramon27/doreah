#!/bin/bash

apt-get install php libssl-dev

git clone https://gitlab.com/Nuramon27/doreah.git

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

cd doreah
git checkout translation_of_final

cargo build --release -p server
cargo run --release -p server &

cd ..
cp doreah/php .
cd php

php -S 127.0.0.1:8010
